DROP DATABASE IF EXISTS TheSeventhTask;

CREATE DATABASE IF NOT EXISTS TheSeventhTask;

USE TheSeventhTask;

CREATE TABLE users (
                       id INT NOT NULL AUTO_INCREMENT PRIMARY KEY,
                       login VARCHAR(255) NOT NULL
);

CREATE TABLE teams (
                       id INT NOT NULL AUTO_INCREMENT PRIMARY KEY,
                       name VARCHAR(255) NOT NULL
);

CREATE TABLE users_teams (
                             user_id INT NOT NULL,
                             team_id INT NOT NULL,
                             PRIMARY KEY (user_id, team_id),
                             FOREIGN KEY (user_id) REFERENCES users(id) ON DELETE CASCADE,
                             FOREIGN KEY (team_id) REFERENCES teams(id) ON DELETE CASCADE
);

INSERT INTO users VALUES (DEFAULT, 'ivanov');
INSERT INTO teams VALUES (DEFAULT, 'teamA');

SELECT * FROM users;
SELECT * FROM teams;
SELECT * FROM users_teams;