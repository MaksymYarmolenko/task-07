package com.epam.rd.java.basic.task7.db;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.sql.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;
import com.epam.rd.java.basic.task7.db.*;

import com.epam.rd.java.basic.task7.db.entity.*;
import com.epam.rd.java.basic.task7.db.queries.Queries;


public class DBManager {

	private static Connection connection;
	private static DBManager instance;

	public static synchronized DBManager getInstance() {
		if (instance == null) {
			instance = new DBManager();
		}

		return instance;
	}

	private DBManager() {}

	public List<User> findAllUsers() throws DBException {
		List<User> users = new ArrayList<>();

		try (Statement s = getConnection().createStatement();
			 ResultSet rs = s.executeQuery(Queries.SQL_GET_ALL_USERS)){

			while (rs.next()) {
				User user = new User();

				int row = 0;

				user.setId(rs.getInt(++row));
				user.setLogin(rs.getString(++row));

				users.add(user);
			}

		} catch (SQLException e) {
			throw new DBException(e.getMessage(), e);
		}

		return users;
	}

	public boolean insertUser(User user) throws DBException {

		try (PreparedStatement ps = getConnection().prepareStatement(Queries.SQL_INSERT_USER, Statement.RETURN_GENERATED_KEYS)){

			ps.setString(1, user.getLogin());

			if (ps.executeUpdate() > 0) {
				try (ResultSet rs = ps.getGeneratedKeys()){
					if (rs.next()) {
						user.setId(rs.getInt(1));

						return true;
					}
				}
			}
		} catch (SQLException e) {
			throw new DBException(e.getMessage(), e);
		}

		return false;
	}

	public boolean deleteUsers(User... users) throws DBException {
		Connection connection = getConnection();

		try (PreparedStatement ps = connection.prepareStatement(Queries.SQL_DELETE_USER)) {
			connection.setAutoCommit(false);

			for (User user : users) {
				ps.setInt(1, user.getId());

				if (ps.executeUpdate() == 0) {
					connection.rollback();
					return false;
				}
			}

			connection.commit();
			connection.setAutoCommit(true);
		} catch (SQLException e) {
			throw new DBException(e.getMessage(), e);
		}

		return true;
	}

	public User getUser(String login) throws DBException {
		ResultSet rs = null;
		User user = null;

		try (PreparedStatement ps = getConnection().prepareStatement(Queries.SQL_GET_USER_BY_LOGIN)) {
			ps.setString(1, login);
			rs = ps.executeQuery();

			if (rs.next()) {
				user = new User();

				int row = 0;

				user.setId(rs.getInt(++row));
				user.setLogin(rs.getString(++row));
			}
		} catch (SQLException e) {
			throw new DBException(e.getMessage(), e);
		}

		return user;
	}

	public Team getTeam(String name) throws DBException {
		ResultSet rs = null;
		Team team = null;

		try (PreparedStatement ps = getConnection().prepareStatement(Queries.SQL_GET_TEAM_BY_NAME)) {
			ps.setString(1, name);
			rs = ps.executeQuery();

			if (rs.next()) {
				team = new Team();

				int row = 0;

				team.setId(rs.getInt(++row));
				team.setName(rs.getString(++row));
			}
		} catch (SQLException e) {
			throw new DBException(e.getMessage(), e);
		}

		return team;
	}

	public List<Team> findAllTeams() throws DBException {
		List<Team> teams = new ArrayList<>();

		try (Statement s = getConnection().createStatement();
			 ResultSet rs = s.executeQuery(Queries.SQL_GET_ALL_TEAMS)){

			while (rs.next()) {
				Team team = new Team();

				int row = 0;

				team.setId(rs.getInt(++row));
				team.setName(rs.getString(++row));

				teams.add(team);
			}

		} catch (SQLException e) {
			throw new DBException(e.getMessage(), e);
		}

		return teams;
	}

	public boolean insertTeam(Team team) throws DBException {
		try (PreparedStatement ps = getConnection().prepareStatement(Queries.SQL_INSERT_TEAM, Statement.RETURN_GENERATED_KEYS)){

			ps.setString(1, team.getName());

			if (ps.executeUpdate() > 0) {
				try (ResultSet rs = ps.getGeneratedKeys()){
					if (rs.next()) {

						team.setId(rs.getInt(1));

						return true;
					}
				}
			}
		} catch (SQLException e) {
			throw new DBException(e.getMessage(), e);
		}

		return false;
	}

	public boolean setTeamsForUser(User user, Team... teams) throws DBException {
		Connection connection = getConnection();

		try (PreparedStatement ps = connection.prepareStatement(Queries.SQL_INSERT_USER_TEAM)){
			connection.setAutoCommit(false);

			for (Team team : teams) {
				int row = 0;

				ps.setInt(++row, user.getId());
				ps.setInt(++row, team.getId());

				if (ps.executeUpdate() == 0) {
					connection.rollback();
					return false;
				}
			}

			connection.commit();
			connection.setAutoCommit(true);
		} catch (SQLException e) {
			try {
				connection.rollback();
			} catch (SQLException ex) {
				throw new DBException(e.getMessage(), e);
			}

			throw new DBException(e.getMessage(), e);
		}

		return true;
	}

	public List<Team> getUserTeams(User user) throws DBException {
		List<Team> userTeams = new ArrayList<>();

		try (PreparedStatement ps = getConnection().prepareStatement(Queries.SQL_GET_ALL_TEAMS_BY_USER)) {
			ps.setInt(1, user.getId());
			ResultSet rs = ps.executeQuery();

			while (rs.next()) {
				Team team = new Team();

				int row = 0;

				team.setId(rs.getInt(++row));
				team.setName(rs.getString(++row));

				userTeams.add(team);
			}
		} catch (SQLException e) {
			throw new DBException(e.getMessage(), e);
		}

		return userTeams;
	}

	public boolean deleteTeam(Team team) throws DBException {

		try (PreparedStatement ps = getConnection().prepareStatement(Queries.SQL_DELETE_TEAM)) {
			ps.setInt(1, team.getId());

			if (ps.executeUpdate() == 0) {
				return false;
			}
		} catch (SQLException e) {
			throw new DBException(e.getMessage(), e);
		}

		return true;
	}

	public boolean updateTeam(Team team) throws DBException {

		try (PreparedStatement ps = getConnection().prepareStatement(Queries.SQL_UPDATE_TEAM)) {
			int row = 0;

			ps.setString(++row, team.getName());
			ps.setInt(++row, team.getId());

			if (ps.executeUpdate() == 0) {
				return false;
			}
		} catch (SQLException e) {
			throw new DBException(e.getMessage(), e);
		}

		return false;
	}

	private String loadProperties() throws DBException {
		Properties properties = new Properties();

		try (InputStream in = new FileInputStream("app.properties")){
			properties.load(in);
		} catch (IOException e) {
			throw new DBException(e.getMessage(), e);
		}

		return properties.getProperty("connection.url");
	}

	private Connection getConnection() throws DBException {
		if (connection == null) {
			try {
				connection = DriverManager.getConnection(loadProperties());
			} catch (SQLException e) {
				throw new DBException(e.getMessage(), e);
			}
		}

		return connection;
	}

}
