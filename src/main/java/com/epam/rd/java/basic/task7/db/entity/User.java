package com.epam.rd.java.basic.task7.db.entity;

import java.util.Objects;

public class User {

	public User() {}

	private User(String login) {
		id = 0;
		this.login = login;
	}

	private int id;

	private String login;

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getLogin() {
		return login;
	}

	public void setLogin(String login) {
		this.login = login;
	}

	public static User createUser(String login) {
		return new User(login);
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) return true;
		if (o == null || getClass() != o.getClass()) return false;
		User user = (User) o;
		return getLogin().equals(user.getLogin());
	}

	@Override
	public int hashCode() {
		return Objects.hash(getLogin());
	}

	@Override
	public String toString() {
		return login;
	}
}
