package com.epam.rd.java.basic.task7.db.queries;

public class Queries {
    public static final String SQL_GET_USER_BY_LOGIN = "SELECT * FROM users WHERE login = ?";

    public static final String SQL_GET_TEAM_BY_NAME = "SELECT * FROM teams WHERE name = ?";

    public static final String SQL_GET_ALL_USERS = "SELECT * FROM users";

    public static final String SQL_GET_ALL_TEAMS = "SELECT * FROM teams";

    public static final String SQL_GET_ALL_TEAMS_BY_USER = "SELECT team_id, t.name FROM users_teams LEFT JOIN teams t on t.id = users_teams.team_id WHERE user_id = ?";

    public static final String SQL_INSERT_USER = "INSERT INTO users VALUES (DEFAULT, ?)";

    public static final String SQL_INSERT_TEAM = "INSERT INTO teams VALUES (DEFAULT, ?)";

    public static final String SQL_INSERT_USER_TEAM = "INSERT INTO users_teams(user_id, team_id) VALUES (?, ?)";

    public static final String SQL_DELETE_USER = "DELETE FROM users WHERE id = ?";

    public static final String SQL_DELETE_TEAM = "DELETE FROM teams WHERE id = ?";

    public static final String SQL_UPDATE_TEAM = "UPDATE teams SET name = (?) WHERE id = ?";
}
